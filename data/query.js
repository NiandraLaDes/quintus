import gql from 'graphql-tag'
import fragments from './fragment'

export const GET_POSTS = gql`
    query getPosts($first: Int, $cursor: String) {
        posts(first:$first, after: $cursor) {
            edges {
                cursor
                node {
                    ...postsFields
                }
            }
            pageInfo {
                endCursor
                hasNextPage
            }
        }
    }
    ${fragments.posts}
`

export const GET_POST = gql`
    query getPost($slug: String!) {
        post: postBy(slug: $slug) {
            title
            content
              featuredImage{
                sourceUrl
              }
        }
    }
`

export const GET_PAGE = gql`
    query getPage($uri: String!) {
        page: pageBy(uri: $uri) {
            date
            title
            content
            featuredImage {
                id
                altText
                sourceUrl
            }
            author {
                id
            }
        }
    }
`

export const GET_PAGE_TITLE = gql`
    query getPageTitle{
        generalSettings {
            description
            title
        }
    }
`

export const GET_MENU = gql`
    query getMenuByLocation($location: MenuLocationEnum = HEADER_MENU) {
        menus(where:{location: $location}) {
            edges{
                node{
                    id
                    name
                    menuItems {
                        edges {
                            node {
                                id
                                label
                                childItems {
                                    edges {
                                        node {
                                            id
                                            label
                                            childItems {
                                                edges {
                                                    node {
                                                        id
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`

export const GET_CATEGORY = gql`
    query getCategory($slug: [String]!) {
        categories(where:{slug:$slug}) {
            edges {
                node {
                    id
                    name
                    description
                    posts {
                        edges {
                            node {
                                ...postsFields
                            }
                        }
                    }
                }
            }
        }
    }
    ${fragments.posts}
`