import gql from 'graphql-tag'

const fragments = {
    posts: gql`
        fragment postsFields on Post {
            id
            title
            excerpt
            date
            uri
            featuredImage {
                altText
                sourceUrl
            }
            author {
                id
                name
            }
        }
    `,

}

export default fragments;