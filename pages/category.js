import React, { Component, Fragment } from "react"
import { Query } from 'react-apollo'
import Category from '../components/Category'
import { GET_CATEGORY } from '../data/query'

class CategoryPage extends Component {
    static async getInitialProps({ query }) {
        const { slug } = query
        return { slug };
    }
    render() {
        return (
            <Query query={GET_CATEGORY} variables={{ slug: this.props.slug }}>
                {({ loading, error, data }) => {
                    if (loading) return "Loading...";
                    if (!error) return <Category category={data.categories.edges[0].node} />;
                    return "No category found";
                }}
            </Query>
        );
    }
}

export default CategoryPage;
