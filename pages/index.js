import { Query } from 'react-apollo'
import PostList from '../components/PostList'
import Loading from '../components/Loading'
import { GET_POSTS } from '../data/query'

const Index = () => (
    <Query query={GET_POSTS}>
        {({ loading, error, data, fetchMore }) => {
            if (loading) return <Loading />;
            if (error) return <div>No posts found</div>;

            const { edges, pageInfo } = data.posts;
            return (
                <PostList
                    posts={edges}
                    onLoadMore={() =>
                        fetchMore({
                            variables: {
                                cursor: pageInfo.endCursor
                            },
                            updateQuery: (previousResult, { fetchMoreResult }) => {
                                const prevEdges = previousResult.posts.edges;
                                const nextEdges = fetchMoreResult.posts.edges;
                                const pageInfo = fetchMoreResult.posts.pageInfo;

                                // Remove posts from `nextEdges` with the same cursor as
                                // in `prevEdges` to avoid duplicated keys
                                const cursors = prevEdges.map(post => post.cursor);
                                const nextEdgesFiltered = nextEdges.filter(post => cursors.indexOf(post.cursor) === -1);

                                return nextEdges.length
                                    ? {
                                        // Put the new posts at the end of the list and update `pageInfo`
                                        // so we have the new `endCursor` and `hasNextPage` values
                                        posts: {
                                            __typename: previousResult.posts.__typename,
                                            edges: [...prevEdges, ...nextEdgesFiltered],
                                            pageInfo
                                        }
                                    }
                                    : previousResult;
                            }
                        })
                    }

                />
            )
        }}
    </Query>
)

export default Index;