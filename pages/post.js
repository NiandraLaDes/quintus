import React, { Component, Fragment } from "react"
import { Query } from 'react-apollo'
import { GET_POST, GET_PAGE } from '../data/query'
import PostItem from '../components/Post'
import Page from '../components/Page'
import Loading from '../components/Loading'

class PagePost extends Component {
    static async getInitialProps({ query }) {
        const { slug } = query;
        return { slug };
    }

    render() {
        return (
            //get post by slug
            <Query query={GET_POST} variables={{ slug: this.props.slug }}>
                {({ loading, error, data }) => {
                    if (loading) return <Loading />;
                    if (!error) return <PostItem post={data.post} />;

                    //no post found so get page by uri
                    return (
                        <Query query={GET_PAGE} variables={{ uri: this.props.slug }}>
                            {({ loading, error, data }) => {
                                if(loading) return <Loading />;
                                if (!error) return <Page page={data.page} />;
                                return '404';
                            }}
                        </Query>
                    )
                }}
            </Query>
        );
    }
}

export default PagePost;
