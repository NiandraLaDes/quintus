import { Fragment } from 'react'
import PropTypes from 'prop-types'

const Image = ( {image} ) => {
    return (
        <Fragment>
            <img src={image ? image.sourceUrl : 'http://localhost:8080/wp-content/uploads/2018/11/noPhotoFound.jpg'} alt={image ? image.altText : ''}/>
            <style jsx>{`
                img {
                    width: 100%;
                }
            `}</style>
        </Fragment>
    )
}

Image.propTypes = {
    image: PropTypes.object
}

export default Image