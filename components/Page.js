import Date from './Date'
import PropTypes from 'prop-types'

const Page = ({ page }) => (
    <article>
        <header>
            <Date date={page.date} />
            <h1>{page.title}</h1>
        </header>
        <section dangerouslySetInnerHTML={{
            __html: page.content
        }}
        />
        <style jsx>{`
        article {
            background: white;
            padding: 2em 3em;
        }
        `}</style>
    </article>
)

Page.propTypes = {
    page: PropTypes.shape({
        date: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired
    })
}

export default Page