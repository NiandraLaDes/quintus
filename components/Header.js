import React, {Component, Fragment} from "react"
import Head from "next/head"
import stylesheet from '../src/styles/style.scss'
import { Query } from 'react-apollo'
import { PAGE_TITLE } from '../data/query'

class Header extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Fragment>
                <Head>
                    <style dangerouslySetInnerHTML={{__html: stylesheet}}/>
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1"
                    />
                    <meta charSet="utf-8"/>
                    <title>
                        Wordpress
                    </title>
                </Head>
                <header></header>
            </Fragment>
        );
    }
}

export default Header
