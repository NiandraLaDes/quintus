import moment from 'moment'
import PropTypes from 'prop-types'

const Date = ({ date }) => (
    <span className="date">
        {moment(date).format('MMMM D, YYYY')}
        <style jsx>{`
        .date {
            display: block;
            margin-bottom: 2em;
            text-align:center;
            font-family: Merriweather, Georgia, serif;
            font-size:1.2em;
        }
        `}</style>
    </span>
)

Date.propTypes = {
    date: PropTypes.string
}

export default Date