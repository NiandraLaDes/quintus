import Link from 'next/link'
import PropTypes from 'prop-types'
import Image from './Image'
import Button from './Button'
import Date from './Date'
import { ApolloConsumer } from 'react-apollo'
import { GET_POST } from '../data/query'

const PostListItem = ({ post }) => {
    const { title, date, featuredImage, excerpt, uri } = post.node;
    return (
        <ApolloConsumer>
            {(client) => (
                <article>
                    <header>
                        <Date date={date} />
                        <h2>{title}</h2>
                    </header>
                    <section>
                        <Image image={featuredImage} />
                    </section>
                    <section
                        dangerouslySetInnerHTML={{
                            __html: excerpt
                        }}
                    />
                    <Link as={`/${uri}`} href={`/post?slug=${uri}&type=post`}>
                        <Button onMouseOver={
                            client.query({
                                query: GET_POST,
                                ssr: false,
                                variables: { slug: uri }
                            })
                        }>Read More</Button>
                    </Link>
                    <style jsx>{`
                        article {
                            border:1px solid #ddd;
                            flex: 0 0 100%;
                            margin: 0 0 .8em 0;
                            padding: 2em;
                            text-align: center;
                            word-break: break-all;
                            background-color: #fff;
                        }

                        header {
                            min-height:150px
                        }

                        h2 {
                            margin-bottom:1.5em;
                            margin-top:0;
                        }

                        @media screen and (min-width: 600px) {
                            article{
                                flex: 0 0 49%;
                            }
                        }

                        @media screen and (min-width: 1300px) {
                            article{
                                flex: 0 0 49%;
                            }
                        }
                    `}</style>
                </article>
            )}
        </ApolloConsumer>
    )
}

PostListItem.propTypes = {
    post: PropTypes.shape({
        title: PropTypes.string,
        date: PropTypes.string,
        featuredImage: PropTypes.object,
        excerpt: PropTypes.string,
        uri: PropTypes.string
    }).isRequired
}

export default PostListItem