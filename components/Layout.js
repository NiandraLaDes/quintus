import { Fragment } from 'react';
import Header from "./Header";
import Nav from "./Nav";
import Footer from "./Footer";
import { Query } from 'react-apollo'
import { GET_MENU } from '../data/query'

const Layout = ({children}) => (
    <div id="wrapper">
        <Header />
        <Query query={GET_MENU}>
            { /* Get main navigation and render if found */ }
            {({ error, data }) => {
                if(!error) {
                    return <Nav menu={data.menus.edges[0].node} />
                }
            }}
        </Query>
        <main>{children}</main>
        <aside></aside>
        <Footer />
        <style jsx global>{`
            #wrapper {
                display:grid;
                grid-template-column: 1fr;
                grid-template-areas:
                    "header"
                    "nav"
                    "main"
                    "footer";
                padding:5em 1em 0;
            }

            header{
                grid-area: header
            }

            nav{
                grid-area: nav
            }

            main {
                grid-area: main;
                min-height: 85vh;
            }

            aside {
                grid-area: aside
            }

            footer {
                grid-area: footer
            }

            @media screen and (min-width:800px) {
                #wrapper {
                    grid-template-columns: 1fr 3fr 1fr;
                    grid-template-areas:
                        "header header header"
                        ". nav ."
                        ". main ."
                        "footer footer footer";
                }
            }
        `}
        </style>
    </div>
);

export default Layout;
