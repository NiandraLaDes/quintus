import Date from './Date'
import Image from './Image'
import PropTypes from 'prop-types'

const Post = ({ post }) => (
    <article>
        <header>
            <Date date={post.date} />
            <h1>{post.title}</h1>
        </header>
        <Image image={post.featuredImage} />
        <section dangerouslySetInnerHTML={{
            __html: post.content
        }} />
        <style jsx>{`
        article {
            background: white;
            padding: 2em 3em;
        }
        `}</style>
    </article>
)

Post.propTypes = {
    post: PropTypes.shape({
        date: PropTypes.string,
        featuredImage: PropTypes.object,
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired
    })
}

export default Post