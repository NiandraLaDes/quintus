const Button = ({ children, onClick }) => {
    return (
        <button onClick={onClick}>
            {children}
            <style jsx>{`
            button {
                background-color: white;
                border: 2px solid black;
                font-weight: bold;
                margin:3em 0 2em;
                padding:1em 2em;
                text-transform: uppercase;
            }

            button:hover {
                background-color:black;
                color:white;
            }
            `}</style>
        </button>
    )
}

export default Button;