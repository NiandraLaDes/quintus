import Link from "next/link";

const Nav = (props) => {
    return (
        <nav>
            <ul>
                <li>
                    <Link as="/" href="/">
                        <a>Home</a>
                    </Link>
                </li>
                <li>
                    <Link as="/about" href={{ pathname: '/post', query: { slug: "about"} }}>
                        <a>About</a>
                    </Link>
                </li>
                <li>
                    <Link as="/category/aciform" href={{ pathname: '/category', query: { slug: 'aciform'} }}>
                        <a>Aciform</a>
                    </Link>
                </li>
                <li>
                    <Link as="/contact" href={{ pathname: '/post', query: { slug: 'contact' } }}>
                        <a>Contact</a>
                    </Link>
                </li>
            </ul>
            <style jsx>{`
            nav {
                padding: 2em 0;
            }

            ul {
                display:flex;
                justify-content: center;
            }

            li {
                padding:0 0.5em;
            }

            a{
                font-weight:600;
            }
            `}</style>
        </nav>
    )
}

export default Nav;
