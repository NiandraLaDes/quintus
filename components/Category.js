import { Fragment } from 'react'
import PropTypes from 'prop-types'
import PostList from './PostList'

const Category = ({ category }) => (
    <article>
        <h1>Category: {category.name}</h1>
        {category.description &&
            <section>
                {category.description}
            </section>
        }
        <section id="posts">
            <h2>Posts</h2>
            {category.posts.edges.length ? <PostList posts={category.posts.edges} /> : 'No posts found'}
        </section>
        <style jsx>{`
        h1, h2 {
            margin-bottom:1em;
        }

        section {
            background: white;
            margin-bottom:2em;
            padding:2em 3em;
        }

        #posts {
            background: none;
            padding: 0;
        }
        `}</style>
    </article>
)

Category.propTypes = {
    category: PropTypes.object.isRequired
}

export default Category;