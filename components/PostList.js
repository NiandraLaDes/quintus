import { Fragment } from 'react'
import PostListItem from './PostListItem'
import Button from './Button'
import PropTypes from 'prop-types'

const PostList = ({ posts, onLoadMore }) => {
    return (
        <Fragment>
            <section>
                {posts.map(post => (
                    <PostListItem key={post.node.id} post={post} />
                ))}
            </section>
            {
                //if `onLoadMore` functionality is provides. Show button (index.js)
                onLoadMore &&
                    <footer>
                        <Button onClick={onLoadMore}>Load More</Button>
                    </footer>
            }
            <style jsx>{`
                section {
                    display:flex;
                    flex-wrap: wrap;
                    justify-content: space-between
                }

                footer {
                    text-align:center;
                }
            `}</style>
        </Fragment>
    )
}

PostList.propTypes = {
    posts: PropTypes.array
}

export default PostList