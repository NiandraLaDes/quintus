import Link from "next/link"

const Footer = () => (
    <footer>
        <Link href="https://enter-webdevelopment.nl">
            <a>Made by Enter Webdevelopment</a>
        </Link>
        <style jsx>{`
            footer {
                text-align:center;
                padding:1em 0;
                font-size:0.8em;
            }

            a:link,
            a:visited {
                color:#000;
            }
        `}</style>
    </footer>
);

export default Footer
