const Loading = () => (
    <section>
        Loading...
        <style jsx>{`
            section {
                text-align:center;
                padding: 1em 0;
            }
        `}</style>
    </section>
)

export default Loading