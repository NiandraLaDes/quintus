# Project Title

Quintus WordPress Theme (WordPress + React + Next.js + GraphQL + Apollo)

# Description

A server-side rendered React theme using [Next.js](https://github.com/zeit/next.js/).
The data is being served by a WordPress backend via the [WP REST API](https://developer.wordpress.org/rest-api/) and [GraphQL](http://graphql.org/)

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces.
* [Next.js](https://nextjs.org/) - A lightweight framework for static and server-rendered applications.
* [GraphQL](http://graphql.org/) - A query language for APIs and a runtime for fulfilling those queries with your existing data.
* [Apollo](https://www.apollographql.com) - Apollo Client is the ultra-flexible, community-driven GraphQL client for React, JavaScript, and native platforms.
* [WordPress](https://wordpress.org/) - A free and open-source content management system based on PHP and MySQL.

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details